use {
    async_trait::async_trait,
    dashmap::DashMap,
    rand::{thread_rng, Rng},
    std::{
        fmt::{self, Display, Formatter},
        hash::Hash,
        sync::Arc,
    },
    thiserror::Error,
    tokio::{
        io::{self, AsyncRead, AsyncReadExt},
        sync::mpsc::{self, error::SendError, Receiver, Sender},
    },
};

const NEW_GUEST_CHANNEL_SIZE: usize = 4;

#[derive(Error, Debug)]
pub enum IdFromStreamError {
    #[error("Failed to read character at position {0}: {1}")]
    FailedToReadChar(usize, io::Error),
    #[error("Failed to parse character at position {0}")]
    FailedToParseChar(usize),
}

#[derive(Hash, PartialEq, Eq, Clone, Copy, Debug)]
pub struct Id<const N: usize> {
    chars: [char; N],
}

impl<const N: usize> Id<N> {
    pub fn new() -> Self {
        Self { chars: ['A'; N] }
    }

    pub fn randomize_with_capital_letters(&mut self, rng: &mut impl Rng) {
        const CHARSET: [char; 26] = [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
            'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        ];
        for i in 0..N {
            let j = rng.gen_range(0..CHARSET.len());
            self.chars[i] = CHARSET[j];
        }
    }

    pub async fn from_stream<T: AsyncRead + Unpin>(src: &mut T) -> Result<Self, IdFromStreamError> {
        let mut chars: [char; N] = ['A'; N];
        for i in 0..N {
            chars[i] = char::from_u32(
                src.read_u32()
                    .await
                    .map_err(|e| IdFromStreamError::FailedToReadChar(i, e))?,
            )
            .ok_or(IdFromStreamError::FailedToParseChar(i))?;
        }
        Ok(Self { chars })
    }

    fn phonetically_eq(a: char, b: char) -> bool {
        if a == b {
            return true;
        }
        if a == 'C' || a == 'c' {
            return b == 'K' || b == 'k';
        }
        if a == 'K' || a == 'k' {
            return b == 'C' || b == 'c';
        }
        false
    }

    fn phonetically_contains<const M: usize>(&self, chars: [char; M]) -> bool {
        if M > N {
            return false;
        }

        for i in 0..(N - M + 1) {
            for j in 0..M {
                if !Self::phonetically_eq(self.chars[i + j], chars[j]) {
                    break;
                }
                if j == M - 1 {
                    return true;
                }
            }
        }

        false
    }

    fn contains_illegal_word(&self) -> bool {
        self.phonetically_contains(['a', 's', 's'])
            || self.phonetically_contains(['c', 'o', 'c'])
            || self.phonetically_contains(['c', 'u', 'c'])
            || self.phonetically_contains(['d', 'i', 'c'])
            || self.phonetically_contains(['f', 'a', 'g'])
            || self.phonetically_contains(['f', 'a', 't'])
            || self.phonetically_contains(['f', 'u', 'c'])
            || self.phonetically_contains(['j', 'a', 'p'])
            || self.phonetically_contains(['j', 'e', 'w'])
            || self.phonetically_contains(['n', 'i', 'g'])
            || self.phonetically_contains(['p', 'e', 'e'])
            || self.phonetically_contains(['p', 'i', 's'])
            || self.phonetically_contains(['b', 'u', 't', 't'])
            || self.phonetically_contains(['c', 'u', 'n', 't'])
            || self.phonetically_contains(['c', 'h', 'i', 'k'])
            || self.phonetically_contains(['k', 'i', 'k', 'e'])
            || self.phonetically_contains(['p', 'h', 'a', 'g'])
            || self.phonetically_contains(['p', 'h', 'a', 't'])
            || self.phonetically_contains(['s', 'h', 'a', 't'])
            || self.phonetically_contains(['s', 'h', 'i', 't'])
    }
}

impl<const N: usize> Display for Id<N> {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "\"")?;
        for i in 0..N {
            write!(f, "{}", self.chars[i])?;
        }
        write!(f, "\"")
    }
}

pub struct Ticket<const N: usize> {
    pub room_id: Id<N>,
    pub room_password: String,
}

pub enum JoinRoomFailure {
    RoomDoesNotExist,
    IncorrectPassword,
}

#[async_trait]
pub trait Stranger<const N: usize> {
    type Error: fmt::Display;
    type Guest;

    async fn get_ticket(&mut self) -> Result<Ticket<N>, Self::Error>;
    async fn get_guest(self) -> Self::Guest;
}

#[derive(Error, Debug)]
pub enum JoinRoomError<const N: usize, S: Stranger<N>> {
    #[error("Failed to get ticket: {0}")]
    FailedToGetTicket(S::Error),
    #[error("Room {0} does not exist")]
    RoomDoesNotExist(Id<N>),
    #[error("Incorrect room password")]
    IncorrectPassword,
    #[error("Failed to send guest to room: {0}")]
    FailedToSendGuestToRoom(SendError<S::Guest>),
}

pub struct Room<G> {
    pub password: String,
    pub new_guests: Sender<G>,
}

impl<G> Room<G> {
    pub fn new(password: String) -> (Self, Receiver<G>) {
        let (tx, rx) = mpsc::channel(NEW_GUEST_CHANNEL_SIZE);
        (
            Room {
                password,
                new_guests: tx,
            },
            rx,
        )
    }
}

pub struct Hub<const N: usize, S: Stranger<N>> {
    public_rooms: Arc<DashMap<Id<N>, Room<S::Guest>>>,
}

impl<const N: usize, S: Stranger<N>> Hub<N, S> {
    pub fn new() -> Self {
        Self {
            public_rooms: Arc::new(DashMap::new()),
        }
    }

    pub async fn join(&self, mut stranger: S) -> Result<(), JoinRoomError<N, S>> {
        let ticket = stranger
            .get_ticket()
            .await
            .map_err(|e| JoinRoomError::FailedToGetTicket(e))?;

        let room = self
            .public_rooms
            .get(&ticket.room_id)
            .ok_or(JoinRoomError::RoomDoesNotExist(ticket.room_id))?;

        if ticket.room_password != room.password {
            return Err(JoinRoomError::IncorrectPassword);
        }

        let guest = stranger.get_guest().await;
        room.new_guests
            .send(guest)
            .await
            .map_err(|e| JoinRoomError::<N, S>::FailedToSendGuestToRoom(e))?;
        Ok(())
    }

    pub fn create_room(&mut self, password: String) -> (Id<N>, Receiver<S::Guest>) {
        let mut rng = thread_rng();
        let mut id = Id::new();
        let (room, new_guests) = Room::new(password);
        while id.contains_illegal_word() || self.public_rooms.contains_key(&id) {
            id.randomize_with_capital_letters(&mut rng);
        }
        self.public_rooms.insert(id, room);

        (id, new_guests)
    }

    pub fn remote_room(&mut self, id: &Id<N>) {
        self.public_rooms.remove(id);
    }
}
